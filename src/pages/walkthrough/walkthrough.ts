import { Component, ViewChild } from '@angular/core';
import {LoadingController, NavController} from 'ionic-angular';

import { GoogleLoginService } from '../google-login/google-login.service';
import { MapsPage } from "../maps/maps";
import {GoogleUserModel} from "../google-login/google-user.model";

@Component({
  selector: 'walkthrough-page',
  templateUrl: 'walkthrough.html'
})
export class WalkthroughPage {
  main_page: { component: any };
  loading: any;
  constructor(public nav: NavController, public googleLoginService: GoogleLoginService, public loadingCtrl: LoadingController) {
    this.main_page = { component: MapsPage };
  }

  doGoogleLogin() {

    this.loading = this.loadingCtrl.create();

    // Here we will check if the user is already logged in because we don't want to ask users to log in each time they open the app

    this.googleLoginService.trySilentLogin()
      .then((data) => {
        // user is previously logged with Google and we have his data we will let him access the app
        console.log("Google Login success 1", data);
        this.nav.setRoot(this.main_page.component);
      }, (error) => {
        //we don't have the user data so we will ask him to log in
        this.googleLoginService.doGoogleLogin()
          .then((res) => {
            this.loading.dismiss();
            console.log("Google Login success", res);
            this.nav.setRoot(this.main_page.component);
          }, (err) => {
            console.log("Google Login error", err);
          });
      });
  }
}

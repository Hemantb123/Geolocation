import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { Geolocation} from '@ionic-native/geolocation';
import { Keyboard } from '@ionic-native/keyboard';
import { GoogleMap } from "../../components/google-map/google-map";
import { GoogleMapsService } from "./maps.service";
import { MapsModel, MapPlace } from './maps.model';
import { WalkthroughPage } from "../walkthrough/walkthrough";
import { GoogleLoginService } from "../google-login/google-login.service";

@Component({
  selector: 'maps-page',
  templateUrl: 'maps.html'
})

export class MapsPage implements OnInit {
  @ViewChild(GoogleMap) _GoogleMap: GoogleMap;
  map_model: MapsModel = new MapsModel();
  toast: any;

  constructor(
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public GoogleMapsService: GoogleMapsService,
    public googleLoginService: GoogleLoginService,
    public geolocation: Geolocation,
    public keyboard: Keyboard
  ) {
  }

  ngOnInit() {
    let _loading = this.loadingCtrl.create();
    _loading.present();

    this._GoogleMap.$mapReady.subscribe(map => {
      this.map_model.init(map);
      _loading.dismiss();
    });
  }

  setOrigin(location: google.maps.LatLng){
    console.log('location', location);
    let env = this;

    // Clean map
    env.map_model.cleanMap();

    // Set the origin for later directions
    env.map_model.directions_origin.location = location;

    env.map_model.addPlaceToMap(location, '#00e9d5');

    env.GoogleMapsService.getPlacesNearby(location).subscribe(
      nearby_places => {
        // Create a location bound to center the map based on the results
        let bound = new google.maps.LatLngBounds();
        console.log('bound', bound);
          bound.extend(location);
        // To fit map with places
        env.map_model.map.fitBounds(bound);
        var listener = google.maps.event.addListener(env.map_model.map, "bounds_changed", function() {
      	  if (env.map_model.map.getZoom()) env.map_model.map.setZoom(16);
      	  // google.maps.event.removeListener(listener);
      	});

      },
      e => {
        console.log('onError: %s', e);
      },
      () => {
        console.log('onCompleted');
      }
    );
  }

  geolocateMe(){
    let env = this,
        _loading = env.loadingCtrl.create();
    _loading.present();

    this.geolocation.watchPosition().subscribe((position) => {
      console.log(position.coords.longitude + ' ' + position.coords.latitude);
      let current_location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      env.setOrigin(current_location);
      _loading.dismiss();
    });
  }

  doGoogleLogout(){
    this.googleLoginService.doGoogleLogout()
      .then((res) => {
        this.nav.setRoot(WalkthroughPage);
      }, (error) => {
        console.log("Google logout error", error);
      });
  }

}

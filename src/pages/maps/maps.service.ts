import { Injectable, NgZone } from "@angular/core";

import { Observable } from 'rxjs/Observable';

@Injectable()
export class GoogleMapsService {
  _PlacesService: google.maps.places.PlacesService;
  constructor(public zone: NgZone) {
    // As we are already using a map, we don't need to pass the map element to the PlacesServices (https://groups.google.com/forum/#!topic/google-maps-js-api-v3/QJ67k-ATuFg)
    this._PlacesService = new google.maps.places.PlacesService(document.createElement("div"));
  }

  getPlacesNearby(location: google.maps.LatLng) : Observable<Array<google.maps.places.PlaceResult>> {
    return Observable.create((observer) => {
      this._PlacesService.nearbySearch({
  	    location: location,
        radius: 500,
        types: ['restaurant']
  	  }, (results, status) => {
        if (status != google.maps.places.PlacesServiceStatus.OK) {
          this.zone.run(() => {
            observer.next([]);
            observer.complete();
          });
  		  }
  			else {
          this.zone.run(() => {
            observer.next(results);
            observer.complete();
          });
  			}
  		});
    });
  }
}
